import styled from "@emotion/styled";
import colors from "../../constants/colors";

export interface ButtonStyle {
    isDisabled: boolean;
}

export const Container = styled.button<ButtonStyle>`
    background-color: ${colors.buttonPrimary};
    border: 2px solid transparent;
    border-radius: 6px;
    color:  black;
    font-size: 18px;
    font-weight: bold;
    font-family: 'Courier New', Courier, monospace;
    min-height: 40px;
    min-width: fit-content;
    transition: all 0.3s;
    width: 100%;

    &:active {
        background-color: ${colors.buttonPressed};
    }

    &:hover {
        border-color: ${colors.chrome};
        color: ${colors.white};
    }
`;