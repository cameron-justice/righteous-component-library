import React, { forwardRef, ReactNode } from 'react';
import { BaseComponentProps } from '../../types';
import { Container } from './Button.styled';

export interface ButtonProps extends BaseComponentProps {
    /**
     * Content to be rendered inside the button.
     */
    children: ReactNode;
    /**
     * Callback for when the button is clicked.
     */
    onClick: VoidFunction;
    /**
     * If true, the button will block all interactions.
     */
    isDisabled?: boolean;
}

const Button = forwardRef<HTMLButtonElement, ButtonProps>(
(
    {
        children,
        clientId,
        isDisabled = false,
        onClick,
    },
    parentRef,
) => {
    return (
        <Container 
            aria-disabled={isDisabled}
            data-client-id={clientId}
            disabled={isDisabled}
            isDisabled={isDisabled}
            onClick={onClick}
            ref={parentRef}
        >
            { 
                children
            }
        </Container>
    );
});

Button.displayName = 'Button';

export default Button;