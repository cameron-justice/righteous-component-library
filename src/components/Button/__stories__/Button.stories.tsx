// import { Meta } from '@stor'
import Button from '../Button';

export default {
    title: 'components/Button',
    component: Button,
}

export const BasicUsage = () => {
    return (
        <Button onClick={() => console.log('Clicked')}>Click Me!</Button>
    )
}