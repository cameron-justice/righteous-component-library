import { css } from "@emotion/react";
import styled from "@emotion/styled";

const PhoneStyle = css`
    background-color: red;
`;

export const Container = styled.input`

    &[type=password] {
        background-color: blue;
    }

    &[type=phone] {
        ${PhoneStyle}
    }
`;