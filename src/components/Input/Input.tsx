import { forwardRef, InputHTMLAttributes } from "react";
import { BaseComponentProps } from "../../types";
import { Container } from './Input.styled';

export interface InputProps 
    extends 
        BaseComponentProps, 
        Pick<InputHTMLAttributes<unknown>, 'type' | 'value' | 'onChange'>
    {
    
}

export const Input = forwardRef<HTMLInputElement, InputProps>(
(
    {
        clientId,
        type,
        value,
        onChange,
        
    },
    parentRef,
) => {
    return (
        <Container 
            data-client-id={clientId}
            type={type}
            value={value}
            onChange={onChange}
        />
    )
});

export default Input;