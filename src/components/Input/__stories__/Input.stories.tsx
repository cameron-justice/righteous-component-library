import React, { HTMLInputTypeAttribute, useState } from 'react';
import { Button } from '../../Button';
import { Input } from "../Input";

export default {
    title: 'Components/Input',
    component: Input
}

export const BasicUsage = () => {
    const [type, setType] = useState<HTMLInputTypeAttribute>('phone');

    return (
        <div>
            {/* TODO: Select component here to select the input type */}
            <Input type={type} />
        </div>
    )
}