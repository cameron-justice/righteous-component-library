import Input from './Input';

export {
    Input
};

export type {
    InputProps
} from './Input';