import styled from "@emotion/styled";
import sizes from "../../constants/sizes";

export const Container = styled.form`
    display: flex;
    flex-direction: column;
    gap: ${sizes.small}px;
`;