import React, { forwardRef, useCallback } from "react";
import { useRecord } from "../../hooks/useRecord";
import { BaseComponentProps } from "../../types";
import { Container } from "./Login.styled";

export type LoginData = {
    username: string;
    password: string;
}

export interface LoginProps extends BaseComponentProps {
    onSubmit: (data: LoginData) => void;
}

export const Login = forwardRef<HTMLFormElement, LoginProps>(
    (
        {
            onSubmit,
        },
        parentRef,
    ) => {
        const { get, set } = useRecord<string>({ username: '', password: '' });

        const handleSubmit = useCallback<React.FormEventHandler>((event) => {
            event.preventDefault();

            onSubmit({ 
                username: get('username'),
                password: get('password'),
            });
        }, []);

        return (
            <Container onSubmit={handleSubmit} ref={parentRef}>
                <input type="text" name="username" onChange={({ target: { value }}) => set('username', value)}/>
                <input type="password" name="password" onChange={({ target: { value }}) => set('password', value)}/>
                <button type="submit">Submit</button>
            </Container>
        )
    }
)

Login.displayName = 'Login';

export default Login;