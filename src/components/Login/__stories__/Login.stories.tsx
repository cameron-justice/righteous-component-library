import { useState } from "react";
import { Login, LoginData } from "../Login";

export default {
    title: 'Components/Login',
    component: Login,
};

export const BasicUsage = () => {
    const [data, setData] = useState<LoginData | null>(null)

    return (
        <div>
            {
                data && (
                    <>
                        <p>Username: {data.username}</p>
                        <p>Password: {data.password}</p>
                    </>
                )
            }
            <Login onSubmit={setData}/>
        </div>
    )
}