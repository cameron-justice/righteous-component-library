import styled from "@emotion/styled";
import colors from "../../constants/colors";
import sizes from "../../constants/sizes";

export const Container = styled.div`
    position: relative;
    min-height: 40px;
    background-color: ${colors.chrome};
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: ${sizes.small}px;
    box-sizing: border-box;
`;

export const Indicator = styled.div`
    
`;

export const Options = styled.div`
    border: ${sizes.xxSmall}px solid ${colors.chrome};
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
`; // TODO: Confirm this element type