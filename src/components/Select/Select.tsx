import React, { FC, forwardRef, MouseEventHandler, ReactNode, Ref, useCallback, useRef, useState } from 'react';
import { BaseComponentProps } from '../../types';
import { Container, Indicator, Options } from './Select.styled';
import Popper from '@mui/material/Popper';
import Option, { OptionBase } from './sub-components/Option';

export interface SelectProps<T extends OptionBase> extends BaseComponentProps {
    options: T[];
    /**
     * Callback to render the option in the 
     */
    renderOption: (option: T) => ReactNode;
    /**
     * Callback for when an option is selected.
     */
    onChange: (option: T) => void;
    /**
     * The currently selected option.
     */
    selectedOption?: T;
}

export const Select = <T extends OptionBase,>({
        clientId,
        options,
        renderOption,
        onChange,
        selectedOption,
    }: SelectProps<T>) => {
        const [isOpen, setIsOpen] = useState(false);
        const popperTarget = useRef(null);

        const handleClick = useCallback<MouseEventHandler<HTMLDivElement>>((event) => {
            event.preventDefault();
            setIsOpen((curr) => !curr);
        }, [])

        const handleSelectOption = useCallback((option: T) => {
            onChange(option);
        }, []);

    return (
        <>
            <Container
                ref={popperTarget}
                onClick={handleClick}
                data-client-id={clientId}
            >
                <span>{ selectedOption ? renderOption(selectedOption) : 'Select' }</span>
                <Indicator>{ isOpen ? '>' : '^' }</Indicator>
            </Container>
            <Popper
                open={isOpen}
                anchorEl={popperTarget.current}
                placement="bottom"
                disablePortal={false}
                modifiers={[
                    {
                        name: 'flip',
                        enabled: true,
                        options: {
                            altBoundary: true,
                            rootBoundary: 'document',
                            padding: 8,
                        },
                    },
                    {
                        name: 'preventOverflow',
                        enabled: true,
                        options: {
                            altAxis: true,
                            altBoundary: true,
                            tether: true,
                            rootBoundary: 'document',
                            padding: 8,
                        },
                    },
                    {
                        name: 'arrow',
                        enabled: false,
                    },
                ]}
            >
                <Options 
                
                >
                    { options.map((o) => <Option onClick={handleSelectOption} option={o}>{ renderOption(o) }</Option> ) }
                </Options>
            </Popper>           
        </>
    )
}