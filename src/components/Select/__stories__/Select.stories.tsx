import { useState } from "react";
import { Select } from "../Select";

export default {
    title: 'Components/Select',
    component: Select,
};

export const BasicUsage = () => {
    const [selected, setSelected] = useState<number | undefined>(undefined);

    return (
        <Select 
            options={[1,2,3]}
            renderOption={(o) => <p>{ o }</p>}
            onChange={setSelected}
            selectedOption={selected}
        />
    )
}