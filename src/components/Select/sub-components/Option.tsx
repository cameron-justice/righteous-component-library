import { FC, MouseEventHandler, ReactNode, useCallback } from "react";
import { BaseComponentProps } from "../../../types";
import { Container } from "./Option.styled";

export type OptionBase =  number | string | Record<any, any>;

export interface OptionProps<T extends OptionBase> extends BaseComponentProps {
    children: ReactNode;
    onClick: (option: T) => void;
    option: T;
}

export const Option = <T extends OptionBase,>({ 
    children,
    onClick,
    option,
}: OptionProps<T>) => {
    const handleClick = useCallback<MouseEventHandler<HTMLOptionElement>>((e) => {
        e.preventDefault();

        onClick(option);
    }, []);

    return (
        <Container
            onClick={handleClick}
        >
            { children }
        </Container>
    )
};

Option.displayName = 'Option';

export default Option;