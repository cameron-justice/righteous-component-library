import React, { forwardRef } from 'react';
import { BaseComponentProps } from '../../types';

export interface TimelineProps extends BaseComponentProps {

}

const Timeline = forwardRef<HTMLDivElement, TimelineProps>(
    (
        {
            clientId,
        },
        parentRef,
    ) => {
        return (
            <div
                ref={parentRef}
                data-client-id={clientId}
            >
                Timeline
            </div>
        )
    }
);

Timeline.displayName = 'Timeline';

export default Timeline;