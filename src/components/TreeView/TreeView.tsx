import React, { forwardRef, ReactNode } from 'react';
import { useCollapsible } from '../../hooks/useCollapsible';
import { useRecord } from '../../hooks/useRecord';
import { BaseComponentProps } from '../../types';

export type TreeNode = { id: string; descendants?: Tree; };

export type Tree = TreeNode[];

const renderTree = (tree: Tree | undefined, renderItem: (id: string) => ReactNode): ReactNode => {
    const { getCollapsibleProps, getToggleProps, isExpanded } = useCollapsible(true);

    if (!tree) {
        return null;
    }

    return (
        <li>
            {
                tree.map((t) => (
                            <ul>
                                <span {...getToggleProps()}>{ renderItem(t.id) } <span>{ isExpanded ? 'Collapse' : 'Expand' }</span></span>
                                <span {...getCollapsibleProps()}>{ isExpanded && renderTree(t.descendants, renderItem) }</span>
                            </ul>
                        )
                )
            }
        </li>
    )
}

export interface TreeViewProps extends BaseComponentProps {
    tree: Tree;
}

export const TreeView = forwardRef<HTMLUListElement, TreeViewProps>(
    (
        {
            tree,
        },
        parentRef,
    ) => {
        return (
            <ul
                ref={parentRef}
            >
                { tree.map((n) => <TreeItem {...n} />) }
            </ul>
        )
    }
);

const TreeItem = ({ id, descendants = [] }: TreeNode) => {
    const { getToggleProps, getCollapsibleProps, isExpanded } = useCollapsible(true);

    return (
        <li>
            <span {...getToggleProps()}>{ id } { isExpanded ? '>' : '^'}</span>
            <div {...getCollapsibleProps()}>
                <TreeView  tree={descendants} />
            </div>
        </li>
    )
}

export default TreeView;