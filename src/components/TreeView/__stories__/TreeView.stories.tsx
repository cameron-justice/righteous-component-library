import TreeView, { Tree } from '../TreeView';

export default {
    title: 'components/TreeView',
    component: TreeView,
}

export const BasicUsage = () => {
    const tree: Tree = [
        {
            id: 'One',
        },
        {
            id: 'Two',
            descendants: [ { id: 'Three' }, { id: 'Four' } ],
        }
    ]
    return (
        <TreeView tree={tree} />
    )
} 