
const primary = '#00a86b';
const primaryDark10 = '#008f5b';
const buttonPrimary = primary;
const buttonPressed = primaryDark10;
const white = '#FFFFFF';
const chrome = '#DBE2E9';

export const colors = {
    buttonPrimary,
    buttonPressed,
    chrome,
    primary,
    primaryDark10,
    white,
};

export default colors;