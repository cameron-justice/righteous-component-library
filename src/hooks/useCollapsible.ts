import React, { CSSProperties, useCallback, useState } from "react";

type ToggleProps = {};
type CollapseProps = {};

interface UseCollapsibleReturn {
    expand: VoidFunction;
    collapse: VoidFunction;
    getToggleProps: () => ToggleProps;
    getCollapsibleProps: () => CollapseProps;
    isExpanded: boolean;
}

const expandedStyles: CSSProperties = {
    height: 'fit-content',
};

const collapsedStyles: CSSProperties = {
    height: 0,
    overflow: 'hidden',
}

export const useCollapsible = (initialIsExpanded: boolean): UseCollapsibleReturn => {
    const [isExpanded, setIsExpanded] = useState(initialIsExpanded);

    const handleToggleClick = useCallback<React.MouseEventHandler>((event) => {
        event.preventDefault();

        setIsExpanded((curr) => !curr);
    }, []);

    const expand = useCallback(() => setIsExpanded(true), []);

    const collapse = useCallback(() => setIsExpanded(false), []);

    const getToggleProps = useCallback<UseCollapsibleReturn['getToggleProps']>(() => {
        return {
            onClick: handleToggleClick,
        };
    }, [handleToggleClick]);

    const getCollapsibleProps = useCallback<UseCollapsibleReturn['getCollapsibleProps']>(() => {
        return {
            style: isExpanded ? expandedStyles : collapsedStyles,
        };
    }, [isExpanded]);

    return {
        expand,
        collapse,
        getToggleProps,
        getCollapsibleProps,
        isExpanded,
    };
};