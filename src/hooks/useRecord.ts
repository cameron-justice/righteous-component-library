import { useCallback, useState } from "react";

type ID = string | number | symbol;

export const useRecord = <T>(initialState: Record<ID, T>) => {
    const [record, setRecord] = useState<Record<ID, T>>(initialState);

    const set = useCallback((id: ID, value: T) => {
        setRecord((curr) => { curr[id] = value; return curr; });
    }, []);

    const get = useCallback((id: ID) => record[id], [record]);

    return {
        set,
        get,
    }
}