export interface BaseComponentProps {
    /**
     * Passthrough data-client-id to be applied to the root element.
     */
    clientId?: string;
}
